# Commands

The `docker` command provides everything you could possible desire to do and much more.
Unfortunately, this leaves you with endless amounts of research to do in order to fully understand how everything works.

That said, there are only a handful of commands that you should become very familiar with in order to get the most out of Docker without diving too far down the rabbit hole.
