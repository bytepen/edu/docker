# docker image

`docker image` is used to manage Docker Images downloaded on your current workstation.

## build

Build an image form a Dockerfile.

This command is very helpful once you start to build your own Dockerfiles to create your own Images.

> Create an Image from the Dockerfile in your current directory
> ```bash,editable
> docker build .
> ```

> Give your Image a REPOSITORY and TAG.
> 
> All this really means is that your image will have a name.
> If you follow up this command with a `docker push`, it will push your Image with this name to your Docker Hub namespace.
> I'll explain this better down at the bottom of this page
> ```bash,editable
> docker build . -t <REPOSITORY>:<TAG>
> ```

## ls

List Images

> List all Docker Images
> ```bash,editable
> docker image ls
> ```

## prune

Remove unused Images.

Sometimes you end up with a ton of Images that you don't actually have any containers for.
You can delete these to clear up some hard drive space.

> Delete all unused Images
> ```bash,editable
> docker image prune
> ```

> You can delete images that haven't been used in the last 30 days with the following command
> ```bash,editable
> docker container prune --filter "until=$((24*30))h"
> ```

## pull

Pull an image or a repository from a registry, such as Docker Hub.

> Download/Update an image from Docker Hub
> ```bash,editable
> docker pull alpine:latest
> ```

## push

Push an image or a repository to a registry, such as Docker Hub.

I explain this better at the bottom of this page.

> Push an image to Docker Hub
> ```bash,editable
> docker push "<username>/<image>:<tag>"
> ```

## rm

Remove one or more Images.

Both `docker image rm <image>` and `docker rmi <image>` do the same thing.

> Delete an image named "my_image"
> ```bash,editable
> docker rmi my_image
> ```

## Deploy an Image to Docker Hub

In order to deploy an image to a repository like Docker Hub so others can easily use it, you need to first create an account [here](https://hub.docker.com/signup).
Once that's completed, you can log into Docker Hub on your command line with the following command.

> Log into Docker Hub on the Command Line.
> You will be prompted for your username and password
> ```bash,editable
> docker login
> ```

Once you're logged in, you're ready to push, however, you need an Image you intend to push first.
Navigate to the folder containing the Dockerfile you wish to deploy and then run the following to tag and build the image.

> Build an Image from the Dockerfile in your current directory named "my_first_image"
> ```bash,editable
> docker build . -t my_first_image:latest
> ```

Now that your container is built, you're ready to deploy it.
You deploy it with the `push` command.

> Push an Image to your Docker Hub namespace
> ```bash,editable
> docker push "<username>/my_first_image:latest"
> ```

Now anyone is able to use your container.
For example, if you go to PlayWithDocker, you can now run the following and get your container

> Create container from Image pushed to your name space
> ```bash,editable
> docker run -it <username>/my_first_image:latest
> ```

