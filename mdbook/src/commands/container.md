# docker container

If you went through the [Introduction](../intro/index.md), the `docker container` Management Command should be familiar.

The `docker container` Management Command is used to manage containers that have already been created from Docker Images.

Running `docker container --help` will show you all of the commands available to you when it comes to managing containers, however, I'll cover the most commonly used ones below.

Finally, it should be noted that while most of these commands are native to the `docker container` Management Command, they've been added as shortcuts to the primary `docker` command.
In other words, instead of running `docker container cp ...`, you can run `docker cp ...`.
This is the most common way you will use these commands, so I will be using the shortcuts as much as possible.

## cp

`docker cp` will allow you to `Copy files/folders between a container and the local filesystem`.
I don't use this one incredibly frequently because I usually know when I'm going to need to copy files in and out of a system, and often opt to just use the volume mapping shown in the [run](#run) section.
That said, when I forget and need to copy in something more than a simple text file, this command is a life saver.

> Copy the `nc` binary into the root of a container. 
> ```bash,editable
> docker cp $(which nc) <container>:/
> ```
> Keep in mind that while this example works within Play with Docker, binaries are usually compiled for specific systems and you will probably run into issues with most binaries.

> Copy a file within a Container to your local filesystem
> ```bash,editable
> docker cp <container>:/tmp/myfile ./file-from-docker
> ```

## exec

`docker exec` will `Run a command in a running container`.
This command is one that I use contantly.
Most frequently, I will use this command to enter into a container that is already running as shown below.

> Enter a running container with a bash shell
> 
> This is super useful if you have a container that's already running, but it's doing something weird and you need to gain access to it
> ```bash,editable
> docker exec -it <container> /bin/bash
> ```

## logs

`docker logs` will `Fetch the logs of a container`.
When using Docker, you will typically either want to run a command and exit immediately or have the Container continuously run in the background.
You will rarely want to view the logs, but when you do need them, they're priceless.

> Continuously view the logs of a running container
> ```bash,editable
> docker logs -f <container>
> ```

## ls

`docker container ls` will `List containers`.
This command can be run a handful of different ways with the same output.
For example, `docker container list` == `docker container ls` == `docker ps`. 
Since `docker ps` is the shortest alias, it is the one you should become familiar with.

> List the Running Containers
> ```bash,editable
> docker ps
> ```
> ```bash
> CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
> 5f3dbc5f03f1        alpine              "tail -f"           56 seconds ago      Up 54 seconds                           infallible_khayyam
> ```

> List ALL Containers
> ```bash,editable
> docker ps -a
> ```
> ```bash
> CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                 PORTS               NAMES
> 5f3dbc5f03f1        alpine              "tail -f"           56 seconds ago      Up 54 seconds                              infallible_khayyam
> da54ac288f1d        ubuntu              "tail -f"           10 minutes ago      Exited (137) 9 minutes                     tender_chandrasekhar
> ```

## prune

`docker container prune` will `Remove all stopped containers`.
I usually avoid this command in favor of removing containers one by one, however, there are times where you'll end up with many containers that you don't care about.
This comes in very handy when your disk is full of Containers you haven't run in a very long time.


> To see this in action, you can run the following commands to create many Exited containers and one Up container
> ```bash,editable
> for i in {1..20}; do docker run alpine; done; docker run -d alpine tail -f
> ```
> Remove all stopped containers
> ```bash,editable
> docker container prune
> ```

> You can delete containers that haven't been started in the last 30 days with the following command:
> ```bash,editable
> docker container prune --filter "until=$((24*30))h"
> ```

## restart

`docker restart` will `Restart one or more containers`.
Much like normal operating systems, occasionally you'll find yourself wanting to restart a Container.

> Restart a Container
> ```bash,editable
> docker restart <container>
> ```

> Drop into a shell and restart when you exit
> ```bash,editable
> docker exec -it <container> /bin/bash; docker restart <container>
> ```

## rm

`docker rm` will `Remove one or more containers`.
This command is very useful when you no longer need to a Container.

> Remove one container
> ```bash,editable
> docker rm <container>
> ```

> Remove multiple containers
> ```bash,editable
> docker rm <container> <container>
> ```

> Kill a Running container
> ```bash,editable
> docker rm -f <container>
> ```

> Note that when using a container id, you only need enough to make it unique
> ```bash
> $ docker ps -a
> CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
> de1ea3eb54e2        alpine              "tail -f"           About a minute ago   Up About a minute                       elegant_sinoussi
> 7db022b3d015        alpine              "tail -f"           11 minutes ago       Up 3 minutes                            optimistic_bassi
> ```
> ```bash,editable
> docker rm -f d
> ```

## run

`docker run` will `Run a command in a new container`.
This one is very important to become familiar with since it defines a container.
While it's technically possible to modify containers after they've been created, it's just easier to get them right the first time.

> Create a container, then remove it as soon as it is exited.
>
> This is insanely helpful while learning or debugging a container you're unfamiliar with.
> ```bash,editable
> docker run --rm <image>
> ```

> Create a container with a specific name
> ```bash,editable
> docker run --name my_name <image>
> ```

> Create a container with a file/directory on your local computer mapped to the docker container.
> 
> This is helpful when you're trying to use a container with a local file.
> For example, imagine you have a `hosts.txt` file filled with IP addresses you want to use docker to run an `nmap -sV -iL hosts.txt` on.
> If you're in the directory that has `hosts.txt`, you can just pass in the current folder with something like -v "$(pwd)":"/host" and use it.
> 
> While that's super helpful, you need to be very careful what you're passing through.
> A common privesc with docker is `docker run --rm -it -v "/":"/host" alpine`, which mounts your local filesystem to `/host` and allows you to edit it as if you have root rights.
>
> ```bash,editable
> docker run -v "/home/user/Documents/docker/mycontainer":"/host" <image>
> ```

> Override the CMD line or append to the ENTRYPOINT line in a Dockerfile
>
> Sometimes you'll find an Image you want to use, but you don't want to enter it as configured
> For example, if you want to use a python container, but you want bash instead of a python shell, you can use this to override the functionality that drops you into the python shell as soon as the container is started.
> ```bash,editable
> docker run -it <image> <command>
> ```
> ```bash,editable
> docker run -it alpine /bin/ash
> ```

## start

`docker start` will `Start one or more stopped containers`.

> Start a stopped container
> ```bash,editable
> docker start <container>
> ```

## stop

`docker stop` will `Stop one or more running containers`.
Note that this command will ask the container to close gently, but after a set period (default of 10 seconds), it will send a `SIGKILL` signal if it isn't stopped yet.
I use this all the time when I'm using containers that run in the background, but that I don't want to run 24/7.

> ```bash,editable
> docker stop <container>
> ```

