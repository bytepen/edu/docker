# Usage

Like I mentioned in the introduction, Docker is commonly used to provide applications in a way that is reliable and easy to spin up. A less common use case that I have been thoroughly enjoying recently is using it to provide me tools. Sometimes tools require a wide variety of dependencies. Sometimes those dependencies clash or need to run in specific environments. With the usage of docker, we can easily set up all of our tools to be segregated, but still work together to bring us to our desired state.

To demonstrate its utility in this type of situation, we will walk through a retired Hack the Box challenge using nothing but Docker to prove its usefulness in a dynamic environment. Up until now we've been playing around with Docker on Digital Ocean, but for this, I'm going to switch over to a system that has all of the tools I need to complete a Hack the Box challenge! That system is Windows 10!... with Docker installed, of course!
