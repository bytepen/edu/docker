# Concepts

Two concepts are often found to be a little confusing and are foundational to the understanding of Docker; Images and Containers.
In this section, I hope to clarify and demystify these concepts.

This section is a little long, but it is very important to understand the difference between Images and Containers.
Knowing the difference will save you a lot of headaches in the long run.

## Images

You can effectively think of a Docker Image as a baseline or template.

For example, if you were to run the command `docker pull hello-world`, you be specifying that you wish to create the Image `hello-world` from Docker Repository such as [Docker Hub](https://hub.docker.com).

Note that if the image `hello-world` has already been pulled, this command could be used to update to the latest version.
This is noteworthy because by default, once a Docker Image has been created, it does not automatically check the Docker Repository or rebuild the Image again.

After running the command above, you will see something similar to the message below:

```bash
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:8c5aeeb6a5f3ba4883347d3747a7249f491766ca1caa47e5da5dfcf6b9b717c0
Status: Downloaded newer image for hello-world:latest
```

After doing this, you can run the command `docker image list` and we will see something similar to the following:

```bash
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
hello-world         latest              bf756fb1ae65        10 months ago       13.3kB
```

From the above information, we can learn the following:

* `REPOSITORY`: Location the Image was created from
* `TAG`: There are many ways to use tags, however, the most popular convention is to use `latest` to signify the most recent release and to provide additional tags that correspond to various versions. For example, you could use `ubuntu:latest` or `ubuntu` to pull the most recently released version or you could use `ubuntu:18.04` to specify exactly Ubuntu Release 18.04.
* `IMAGE ID`: Docker assigns each image a randomly generated id to guarantee unique identification
* `CREATED`: This tells us when the source repository last updated the Image we pulled
* `SIZE`: This tells us the cumulative amount of space taken up by the Image and its Parent Images. As explained in a moment, Docker Images are built from layers. These layers are reused when possible. This means that if you have two images where one is based `FROM` another, and the first says `200MB` and the second says `300MB`, you are not using `500MB` on your hard drive, instead, you are using `200MB` for the first Image, and then an additional `100MB` for the second Image

Images are defined using files with the name `Dockerfile`.
When someone creates a `Dockerfile`, they can build and push it to a Docker Repository like [Docker Hub](https://hub.docker.com).
Other people are then able to use the creator's work either directly or as a starting place for their own `Dockerfile`.

As an example, consider the following `Dockerfile` contents:

```dockerfile,editable
FROM alpine:latest
RUN apk add python3
CMD /bin/ash
```

Each line represents a `layer` used in the creation of an Image.

The `FROM` line indicates that the Image we are creating is using the `alpine:latest` Image as a place to start from.

The `RUN` line indicates that we intend to execute the command `apk add python3` to include python within our new Image.

Finally, the `CMD` line indicates that we wish to be entered into the `/bin/ash` shell when Docker provisions a Container based on this Image.

If these three lines were saved as a `Dockerfile`, we could execute `docker build . -t my-python` to indicate that we wanted to build the `Dockerfile` in the current directory (`.`) into an Image named `my-python`.


```bash
$ cat Dockerfile
FROM alpine:latest
RUN apk add python3
CMD /bin/ash

$ docker build . -t my-python
Sending build context to Docker daemon  46.79MB
Step 1/3 : FROM alpine:latest
latest: Pulling from library/alpine
188c0c94c7c5: Pull complete 
Digest: sha256:c0e9560cda118f9ec63ddefb4a173a2b2a0347082d7dff7dc14272e7841a5b5a
Status: Downloaded newer image for alpine:latest
 ---> d6e46aa2470d
Step 2/3 : RUN apk add python3
 ---> Running in 429cfeea1804
fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/community/x86_64/APKINDEX.tar.gz
(1/10) Installing libbz2 (1.0.8-r1)
(2/10) Installing expat (2.2.9-r1)
(3/10) Installing libffi (3.3-r2)
(4/10) Installing gdbm (1.13-r1)
(5/10) Installing xz-libs (5.2.5-r0)
(6/10) Installing ncurses-terminfo-base (6.2_p20200523-r0)
(7/10) Installing ncurses-libs (6.2_p20200523-r0)
(8/10) Installing readline (8.0.4-r0)
(9/10) Installing sqlite-libs (3.32.1-r0)
(10/10) Installing python3 (3.8.5-r0)
Executing busybox-1.31.1-r19.trigger
OK: 53 MiB in 24 packages
Removing intermediate container 429cfeea1804
 ---> 7f9e2366af56
Step 3/3 : CMD /bin/ash
 ---> Running in 0fd76024f4cc
Removing intermediate container 0fd76024f4cc
 ---> 8d074c420c55
Successfully built 8d074c420c55
Successfully tagged my-python:latest

$ docker image list
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my-python           latest              8d074c420c55        6 seconds ago       50.7MB
alpine              latest              d6e46aa2470d        3 weeks ago         5.57MB
```

Note that the `alpine:latest` Image was created first, then the `my-python` image was created.

Finally, let's add another layer after our initial `RUN` layer as shown below:

```dockerfile,editable
FROM alpine:latest
RUN apk add python3
RUN apk add py3-pip && \
    pip3 install bs4
CMD /usr/bin/env python3
```

These changes will install `pip3` and the python module `bs4`.
We also changed the `CMD` layer so that it will drop us directly into a `python3` shell.

Now if we run `docker build . -t beautiful-soup`, we will see the following:

```bash
Sending build context to Docker daemon  46.79MB
Step 1/4 : FROM alpine:latest
 ---> d6e46aa2470d
Step 2/4 : RUN apk add python3
 ---> Using cache
 ---> 7f9e2366af56
Step 3/4 : RUN apk add py3-pip &&     pip3 install bs4
 ---> Running in 5c0c3218af14
(1/26) Installing py3-appdirs (1.4.4-r1)
(2/26) Installing py3-ordered-set (4.0.1-r0)
(3/26) Installing py3-parsing (2.4.7-r0)
(4/26) Installing py3-six (1.15.0-r0)
(5/26) Installing py3-packaging (20.4-r0)
(6/26) Installing py3-setuptools (47.0.0-r0)
(7/26) Installing py3-chardet (3.0.4-r4)
(8/26) Installing py3-idna (2.9-r0)
(9/26) Installing py3-certifi (2020.4.5.1-r0)
(10/26) Installing py3-urllib3 (1.25.9-r0)
(11/26) Installing py3-requests (2.23.0-r0)
(12/26) Installing py3-msgpack (1.0.0-r0)
(13/26) Installing py3-lockfile (0.12.2-r3)
(14/26) Installing py3-cachecontrol (0.12.6-r0)
(15/26) Installing py3-colorama (0.4.3-r0)
(16/26) Installing py3-distlib (0.3.0-r0)
(17/26) Installing py3-distro (1.5.0-r1)
(18/26) Installing py3-webencodings (0.5.1-r3)
(19/26) Installing py3-html5lib (1.0.1-r4)
(20/26) Installing py3-pytoml (0.1.21-r0)
(21/26) Installing py3-pep517 (0.8.2-r0)
(22/26) Installing py3-progress (1.5-r0)
(23/26) Installing py3-toml (0.10.1-r0)
(24/26) Installing py3-retrying (1.3.3-r0)
(25/26) Installing py3-contextlib2 (0.6.0-r0)
(26/26) Installing py3-pip (20.1.1-r0)
Executing busybox-1.31.1-r19.trigger
OK: 65 MiB in 50 packages
Collecting bs4
  Downloading bs4-0.0.1.tar.gz (1.1 kB)
Collecting beautifulsoup4
  Downloading beautifulsoup4-4.9.3-py3-none-any.whl (115 kB)
Collecting soupsieve>1.2; python_version >= "3.0"
  Downloading soupsieve-2.0.1-py3-none-any.whl (32 kB)
Using legacy setup.py install for bs4, since package 'wheel' is not installed.
Installing collected packages: soupsieve, beautifulsoup4, bs4
    Running setup.py install for bs4: started
    Running setup.py install for bs4: finished with status 'done'
Successfully installed beautifulsoup4-4.9.3 bs4-0.0.1 soupsieve-2.0.1
Removing intermediate container 5c0c3218af14
 ---> 227a31be041f
Step 4/4 : CMD /usr/bin/env python3
 ---> Running in 2338f2842c76
Removing intermediate container 2338f2842c76
 ---> 0049ceb6f155
Successfully built 0049ceb6f155
Successfully tagged beautiful-soup:latest
```

If we run `docker image list`, we will see the following:

```bash
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
beautiful-soup      latest              0049ceb6f155        2 minutes ago       61.6MB
my-python           latest              8d074c420c55        12 minutes ago      50.7MB
alpine              latest              d6e46aa2470d        3 weeks ago         5.57MB
```

We will see that Steps 1 and 2 reference our already created Images and the steps are not performed again.
This is VERY handy when you are using many Images that are similar and large or very time-consuming.
To reiterate a point from earlier, we should note that the `SIZE` column says `61.6MB`, `50.7MB`, and `5.57MB`, however, if we run `du -hs /var/lib/docker/overlay2`, we can see that the total size of all of our Images is only `67.4MB`.
We can run `du -hs /var/lib/docker/overlay2/*` to see the individual sizes of all Images, which are `13.0MB`, `48.9MB`, and `5.6MB` respectively.


```bash
13.0M   /var/lib/docker/overlay2/154bcede905a395610808594285b80c8eb254ddbf33da3145d79965a401300a8
5.6M    /var/lib/docker/overlay2/5c78b3c08ff5d75a51e9a314bc0f455a29b18bca72275ed346eafdecd2bcbe76
48.9M   /var/lib/docker/overlay2/934a3cb2abc3810d3b5e8d798952fbe43256b2d1f028c3237fbf6ed570f3155f
0       /var/lib/docker/overlay2/backingFsBlockDev
0       /var/lib/docker/overlay2/l
```

## Containers

While not totally correct, you can effectively think of a Docker Container as a Virtual Machine.
In this analogy, a Container is created from an Image much as a Virtual Machine could be created from a Snapshot.

We are able to use the `docker run` command to create a Container by specifying an Image it should be based on. For example, if we execute `docker run hello-world`, we will provision a Docker container from the Image `hello-world` and execute it.


```bash
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:8c5aeeb6a5f3ba4883347d3747a7249f491766ca1caa47e5da5dfcf6b9b717c0
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```


If you have not already pulled the `hello-world` Image, it will be pulled automatically from your default Docker Repository, such as Docker Hub.
This can be seen in the top 5 lines of the above output.

After the Image has become available, it will execute.
Next time you run `docker run hello-world`, it will execute immediately since we already have the Image.

```bash
$ docker run hello-world

Hello from Docker!
...
```

Now that our Container has been created, we can use the `docker container list` command to view our containers.
Note that unlike Images, Containers can have various statuses, such as `Up` or `Exited`.
By default, only Containers that are currently running are shown with `docker container list`.
To see our `hello-world` containers, which have been `Exited`, we need to execute `docker container list --all`:

```bash
$ docker container list --all
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
837c44637587        hello-world         "/hello"            7 seconds ago       Exited (0) 6 seconds ago                        happy_taussig
07b6dc23212f        hello-world         "/hello"            11 seconds ago      Exited (0) 10 seconds ago                       heuristic_gauss
```

There are a handful of attributes listed with each Container.

* `CONTAINER ID`: Much like `IMAGE ID`, this is an id assigned by Docker to uniquely identify Containers
* `IMAGE`: This is the Image that the Container was based off of
* `COMMAND`: This is the command that was used to execute the Docker Container. Commands can be entered manually or defined in a `Dockerfile` through a layer such as `CMD`
* `CREATED`: This tells us how long ago the image was first created
* `STATUS`: This is the current status of the Container, such as `Up` or `Exited` along with how long it has been in its current status
* `PORTS`: One major feature of Docker is the ability to map ports within a Container to ports on your machine. For example, if your Container used `port 80` to provide a web interface, you could map it to `port 8080` of your local machine and (firewalls permitting), other users on your network could use the Container's service by visiting `http://<your_ip>:8080`. This will be discussed later
* `NAMES`: While they `CONTAINER ID` is great, it isn't very human readable. That's where `NAMES` comes in. When creating a Container, you can assign it a name to help you more easily identify it in the future. If you do not provide a name, one will be created for your in the form of `adjective_noun`. This will be discussed in depth later

To understand the `COMMAND` better, let's run our python Images we created earlier.
When we run `docker run -it my-python`, we should notice that we are dropped into the `/bin/ash` shell as can be confirmed by running `ps`.
`-i` tells Docker to keep `STDIN open` and `-t` tells Docker to allocate a `pseudo-tty`.
An easy, though not totally accurate, way to remember this common argument is `Interactive Terminal`.

```bash
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 /bin/ash
    9 root      0:00 ps
```

I want to take this opportunity to point out that Process ID (PID) `1` here is `/bin/ash`.
In most Linux setups, PID `1` is the initialization system, such as `init`, or `systemd`.
Docker does not use a traditional initialization system to launch all processes.
Instead, the command that is used to provision the Container is assigned PID `1` and all other processes are spawned from this process.
When this process ends, the Container is exited.

Exit that container and run `docker run -it beautiful-soup`.
Notice that this time, we are dropped into a python shell.
This is because we changed the `CMD` layer to `CMD /usr/bin/env python3`.

```bash
$ docker run -it beautiful-soup
Python 3.8.5 (default, Jul 20 2020, 23:11:29) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

Run `exit()` to leave the container and `docker container list --all` to show something similar to the following:

```bash
$ docker container list --all
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                         PORTS               NAMES
08e6e5070ef3        beautiful-soup      "/bin/sh -c '/usr/bi…"   2 minutes ago       Exited (0) 4 seconds ago                           amazing_ganguly
1e74d42abb17        my-python           "/bin/sh -c /bin/ash"    21 minutes ago      Exited (0) 2 minutes ago                           happy_kalam
837c44637587        hello-world         "/hello"                 About an hour ago   Exited (0) About an hour ago                       happy_taussig
07b6dc23212f        hello-world         "/hello"                 About an hour ago   Exited (0) About an hour ago                       heuristic_gauss
```

## Clean Up

Now that we have a handful of Images and Containers, I want to talk about deleting them when they're no longer needed.

We can remove images with the `docker (container|image) rm <id>` command. For example, `docker container rm 08e6e5070ef3` or `docker container rm amazing_ganguly`.
Note that when using the `CONTAINER ID`, you only need to type enough to resolve to a single `CONTAINER ID`.
For example, `0` could resolve to either `08e6e5070ef3` or `07b6dc23212f` and would not work, but `08` can only resolve to `08e6e5070ef3`, so you would be fine to run `docker container rm 08`.

It should be noted that items with dependencies cannot be deleted until their dependent items have been deleted.
For example, all Containers depend on their Images, therefore, no Images can be deleted until their associated containers have been deleted.

```bash
$ docker image rm my-python
Error response from daemon: conflict: unable to remove repository reference "my-python" (must force) - container 1e74d42abb17 is using its referenced image df9642bc5195
$ docker container rm 1e74d42abb17
1e74d42abb17
$ docker image rm my-python
Untagged: my-python:latest
```

## Separation of Responsibilities

Ideally, each Docker Container should have its own area of responsibility.

As an example, if you are creating a web application with a React front end, a Django back end, and a mysql Database, the most "Docker" way to do things would be to create three containers that deal with each discipline and tie them together using something like Docker Compose as we discuss later.

## Summary

Hopefully you found the above information useful and hopefully you're still with me!
I honestly intend for this section to be the longest since having a good foundational understanding of these two concepts will really help everything else make sense.

To recap, there are two main concepts you need to understand with Docker:
* Images
	* Essentially baselines/templates
	* Your Images can be seen with `docker image list`
	* You can get a new Image with `docker pull <image_name>`
* Containers
	* Runnable Instance based on an Image
	* Your Containers can be seen with `docker container list --all`
	* You can start a new container with `docker run <image_name>`
		* If you want the Container to run in the foreground, you would usually run `docker run -it <image name>
