# Docker Hub

Docker Hub is like an "App Store" for Docker Images.

It is the default location that Docker will check when you execute a command like `docker pull <image_name>`.

Anyone can register an account and share their work for others to pull.
This is great for collaboration and sharing, but could potentially lead to a supply chain injection attack where once trusted code has been modified to become malicious.
Therefore, it is always essential that you trust the source of any Images on your machine.

I encourage you to go to [Docker Hub](https://hub.docker.com/) and search for some containers you're interested in.
For example, if you search for [minecraft](https://hub.docker.com/search?q=minecraft&type=image), you will see a plethora of minecraft Images that you could easily run to create your own minecraft server.

Another point of interest is that when a project is hosted on Github, you can see its Dockerfile on Docker Hub as is the case with [itzg/minecraft-server](https://hub.docker.com/r/itzg/minecraft-server/dockerfile) and their corresponding [GitLab Repository](https://github.com/itzg/docker-minecraft-server).
While not a full-proof method of verifying your sources, it can provide a very good indication of how trustworthy an Image is and what exactly it would be running.
