# Workflow

While the concepts were explained in depth in the [concepts](./concepts.md) page, I figured I would walk through the workflow much quicker here.

## Define Task

I want to have a `mysql` server.

## Locate Docker Image

I go to [Docker Hub](https://hub.docker.com) and search for [mysql](https://hub.docker.com/search?q=mysql&type=image).
I find the official image for `mysql`.
I understand that it is very likely that I can trust this Image as it has been officially blessed by Docker as shown by the `Official Image` badge and has millions of downloads and thousands of stars.

![mysql](../../images/mysql-official.png)

I go to the [mysql page](https://hub.docker.com/_/mysql) and can review the tags available to me as well as usage instructions.

I want to ensure that this database is highly stable and don't want to risk it breaking with an update, so I may opt to choose the tag `5.7`.

## Pull Docker Image

I pull the image with the following command:

```bash
$ docker pull mysql:5.7
5.7: Pulling from library/mysql
bb79b6b2107f: Pull complete 
49e22f6fb9f7: Pull complete 
842b1255668c: Pull complete 
9f48d1f43000: Pull complete 
c693f0615bce: Pull complete 
8a621b9dbed2: Pull complete 
0807d32aef13: Pull complete 
f15d42f48bd9: Pull complete 
098ceecc0c8d: Pull complete 
b6fead9737bc: Pull complete 
351d223d3d76: Pull complete 
Digest: sha256:4d2b34e99c14edb99cdd95ddad4d9aa7ea3f2c4405ff0c3509a29dc40bcb10ef
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7
```

I confirm that it is now available to me with the following command:

```bash
$ docker image list
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mysql               5.7                 1b12f2e9257b        3 weeks ago         448MB
```

## Create Container

After referring to the documentation that I read on [the mysql page on Docker Hub](https://hub.docker.com/_/mysql), I determine that I should use the following command to create my Docker Container:

```bash
$ docker run --name mysql-test -e MYSQL_ROOT_PASSWORD=S00p3rSecret -d mysql
43a84b92f7da085769b9c501f35488ecbc3e56c61e2f00dfa13cad510f21cf14
```

I see the above output and can confirm that it was started with the following command:

```bash
$ docker container list
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                 NAMES
43a84b92f7da        mysql:5.7           "docker-entrypoint.s…"   About a minute ago   Up About a minute   3306/tcp, 33060/tcp   mysql-test
```

I note that `CONTAINER ID` is the first handful of characters from the output of the `docker run` command, it is based on the `mysql:5.7` Image, it is `Up`, and it opened ports `3306/tcp` and `33060/tcp` by default.
Additionally, it is named `mysql-test` since I specified that in my command.

## Use Container

Now I can use it by using the `docker exec` command to indicate that I would like to execute `mysql -p` within the Container.

```bash
$ docker exec -it mysql-test mysql -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.32 MySQL Community Server (GPL)

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> CREATE DATABASE test;
Query OK, 1 row affected (0.00 sec)
```

From here, I can use the container as I wish.

## Destroy Container and Image

I am now finished with the container and wish to delete it and its associated Image.
I can do so with the following commands:

```bash
$ docker container stop mysql-test
mysql-test
$ docker container rm mysql-test
mysql-test
$ docker image rm mysql:5.7
Untagged: mysql:5.7
Untagged: mysql@sha256:4d2b34e99c14edb99cdd95ddad4d9aa7ea3f2c4405ff0c3509a29dc40bcb10ef
Deleted: sha256:1b12f2e9257be96da5c075f186dc371b18442b0f9281728ac64c9a69c6e0e264
Deleted: sha256:b21d85dcc43f7db4e532c3ca07e5adbc1e02beb37646b7079217ea2b1922af91
Deleted: sha256:603c9faa831d58a3f60eb377cd6246a281a97b906406a92bf3452726a6f15b69
Deleted: sha256:2d927a6eb4cc26ba5454193a3f4abf503b6cea710293d4f0e6d061c08578b687
Deleted: sha256:b96c2c2176ca7b5223469ad39c9e17509505233dd81030878bc12b03074baef4
Deleted: sha256:33134afe9e842a2898e36966f222fcddcdb2ab42e65dcdc581a4b38b2852c2e0
Deleted: sha256:dd053ec71039c3646324372061452778609b9ba42d1501f6a72c0272c94f8965
Deleted: sha256:2d4c647f875f6256f210459e5c401aad27ad223d0fa3bada7c4088a6040f8ba4
Deleted: sha256:4bded7e9aa769cb0560e60da97421e2314fa896c719179b926318640324d7932
Deleted: sha256:5fd9447ef700dfe5975c4cba51d3c9f2e757b34782fe145c1d7a12dfbee1da2f
Deleted: sha256:5ee7cbb203f3a7e90fe67330905c28e394e595ea2f4aa23b5d3a06534a960553
Deleted: sha256:d0fe97fa8b8cefdffcef1d62b65aba51a6c87b6679628a2b50fc6a7a579f764c
```
