# Introduction

Welcome to my Intro to Docker course! This is by no means intended to be the end-all be-all guide to Docker. What this is intended to be is a cursory introduction to docker to teach you just enough to be dangerous. Ideally, this should be about an hour's worth of material at most.

We will start of be explaining at a high level how Docker works and some of the basic concepts. From there, we will explain how to utilize Docker within the command line. Then we will walk through basic Docker orchestration and how you can create your own Docker Image. Finally, we will conclude with a full walk through of a Hack the Box challenge using nothing but tools available to us via Docker.
