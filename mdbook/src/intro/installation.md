# Installation

Docker can be installed on nearly anything from your standard Windows/macOS/Linux boxes down to simple devices such as the Raspberry Pi.
For the purpose of this guide, I want to get started quickly in a way that you can follow along with for free.
Nearly all of the concepts I will cover will be possible with Play With Docker as described below.
As such, I will be writing the instructional section of this documentation with expectation that you are using Play with Docker.

If you intend to follow along with the Hack the Box example at the end of this lesson, you will need to set up Docker on the workstation you access Hack the Box from.

## Play with Docker

You will first need a Docker Hub account.
If you do not have one, you can get one [here](https://hub.docker.com/signup).

Once you have an account, go to [Play with Docker](https://labs.play-with-docker.com/) and log in if necessary.
You will see a green `Start` button. Click it and you will see a screen similar to the following.

![Play with Docker](../../images/play-with-docker.png)

Click on `Add New Instance` and you will be provisioned a sandbox environment.
Within this sandbox, you can run all standard Docker commands.
Do note that after 4 hours, your sandbox will be destroyed and 

![Play with Docker 2](../../images/play-with-docker-2.png)

While reading this material, I highly encourage you to practice everything here as anything you do be unable to affect your personal machine.
In the worst case scenario, you can `Close Session` and start all over again within seconds.

# Actual Installation

In the real world, Play with Docker will be less useful and you will probably want to have Docker installed on a machine you own.
Luckily, it's super easy to install Docker on just about anything. I'll briefly cover a few of the most common options here.

## Windows

I highly recommend checking out the official [Windows Docker Installation documentation](https://docs.docker.com/docker-for-windows/install/) for the most up-to-date instructions.

At a high level, Docker Desktop for Windows is a GUI based management solution for Docker.
By installing it, you are able to manage all of your Docker containers in an easy to use GUI.
Additionally, installing Docker Desktop will provide you with the CLI `docker` command to allow for more advanced manipulation.

## macOS

I highly recommend checking out the official [Mac Docker Installation documentation](https://docs.docker.com/docker-for-mac/install/) for the most up-to-date instructions.

This provides the same Docker Desktop utility and `docker` CLI command that a Windows installation provides.

## Linux

I highly recommend checking out the official Linux Docker Installation documentation for [CentOS](https://docs.docker.com/engine/install/centos/), [Debian](https://docs.docker.com/engine/install/debian/), [Fedora](https://docs.docker.com/engine/install/fedora/), [Ubuntu](https://docs.docker.com/engine/install/ubuntu/), or [Other Distros](https://docs.docker.com/engine/install/binaries/).

As is often the case with Linux, you don't get the nice GUI that comes with Docker Desktop, however, you do get the same `docker` CLI command.
