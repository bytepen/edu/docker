# Docker Compose

Docker Compose is a template that ties together full suites of Dockerfiles and Docker Images to create a complex application or network at the execution of a single command.

Docker Desktop for Windows and macOS includes this by default, but there are additional setup steps for Linux, which you can find [here](https://github.com/docker/compose).

A fairly simple `docker-compose.yml` file may look like the following:

```yaml,editable
version: "3.8"
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
  redis:
    image: redis
volumes:
  logvolume01: {}
```

This will start two containers.
One is built from a `Dockerfile` in the current directory as seen by `build: .` under `web`.
It opens port 5000 as seen in the `ports` section and mounts the current working directory and log directory as seen in the `volumes` section under `web`.

It also spins up a `redis` server that the web server would use for data storage.

If you're in the directory that contains this `docker-compose.yml` file, you can simply run `docker-compose up` and Docker will read the file and build your environment according the the specifications.

`docker-compose.yml` files have MANY options and complexities, but hopefully this was enough to give you a very basic understanding.

You can find out more information about Docker Compose in the [Official Documentation](https://docs.docker.com/compose/).
