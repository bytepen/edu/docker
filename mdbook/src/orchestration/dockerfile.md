# Dockerfile

Dockerfiles are simple definitions of what a Docker Image should be based on and how it should be configured.

A very simple example of a Dockerfile that takes the base `alpine:latest` Image and adds the `curl` package to it, then drops you into a `/bin/ash` shell when you execute it in interactive mode would be as follows:

```bash,editable
FROM alpine:latest

RUN apk update && \
    apk add curl

CMD /bin/ash
```

The first line (`FROM`) indicates what you will be using as a base image.
This can be literally any Image that is either pushed to your configured Docker registry or exists on your local machine.

The second line (`RUN`) indicates that you wish to install `curl`.
Note that you can break lines onto multiple lines for better readability.

The last lines (`CMD`) indicates that by default, you should be dropped into a `/bin/ash` shell when you execute it in interactive mode.

There are a TON of different arguments that you can use within a Dockerfile to achieve various outcomes, but there are a few overall concepts that you should know before you start building your own Dockerfiles.

The primary thing to keep in mind is that Docker Images are built in "layers" and each line in a Dockerfile corresponds to a single "layer".
Layers are reusable between images, so if you have a handful of Images that are based on `alpine:latest` and they all require the same handful of base packages, it may be beneficial to keep the first few layers the same, and then start to differentiate between them in later layers.
As an alternative, you could create an image at `<username>/base_alpine:latest` and then use that in the `FROM` layer for your various Dockerfiles.

For more information on Dockerfiles, I suggest you check out the [Official Documentation](https://docs.docker.com/engine/reference/builder/).
